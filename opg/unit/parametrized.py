#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on 
http://blog.csdn.net/jasonwoolf/article/details/47979655
@author: li.taojun
'''

import unittest
class ParametrizedTestCase(unittest.case.TestCase):
    """ 
        TestCase classes that want to be parametrized should
        inherit from this class.
    """
    def __init__(self, methodName='runTest', param=None):
        super(ParametrizedTestCase, self).__init__(methodName)
        self.param = param
    def getInputData(self):
        return self.param[5]
    def getCaseid(self):
        return self.param[0]
    def getTestPoint(self):
        return self.param[1]
    def getExpectData(self):
        return self.param[6]

    @staticmethod
    #===========================================================================
    # parametrize
    #根据测试类（继承了ParametrizedTestCase）和测试数据（从excel读取）构成TestCase
    #===========================================================================
    def parametrize(testcase_klass, params={}):
        """ Create a suite containing all tests taken from the given
            subclass, passing them the parameter 'param'.
        """
        suite = unittest.TestSuite()
        testnames = params.keys()
        for name in testnames:
            casels = params[name]
            for onecase in casels:
                 if hasattr(testcase_klass, name):
                     suite.addTest(testcase_klass(name,onecase))
                 else:
                     print("%s类不存在%s方法" % (testcase_klass.__name__,name))
        return suite
    def id(self):
        return "%s.%s_%s" % (self.__interfaceName__+"--"+self.param[0],self.param[0],self.param[2])
#     return "%s.%s_%s" % (self.__interfaceName__+"--"+self.param[0],self.param[0],self.param[4]+"--"+self.param[2])
#     def __repr__(self):
#         return "<%s testMethod=%s>" % \
#                (self.param[1], self.param[0])
#     def __str__(self):
#         return "%s (%s)" % (self.param[1], self.param[0])
#####################################################
##-testcase
#####################################################
class TestOneZgr(ParametrizedTestCase):
    __interfaceName__ = "baomingzgr"
#     def __init__(self,num):
#         self.num = num
    def test_something(self):
        #print 'param =', self.param
        self.assertEqual(1, 1)

    def test_something_else(self):
        self.assertEqual(2, 2)

if __name__ == "__main__":
    suite = unittest.TestSuite()
    suite.addTest(ParametrizedTestCase.parametrize(TestOneZgr, params={"test_something":[6,22,33]}))
    suite.addTest(ParametrizedTestCase.parametrize(TestOneZgr, params={"test_something_else":[6,22,33]}))
    unittest.TextTestRunner(verbosity=2).run(suite)